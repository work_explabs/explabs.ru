import Vue from 'vue'
import moment from 'moment';
Vue.prototype.$moment = moment

Vue.filter('formatDate', function (value) {
    if (!value) return ''
    value = moment(value).format('DD.MM.YYYY HH:mm')
    return value
})

Vue.filter('formatDateWithTextMonth', function (value) {
  if (!value) return ''
  value = moment(value).format('DD MMMM YYYY')
  return value
})

Vue.filter('formatDateLLL', function (value) {
  if (!value) return ''
  value = moment(value).format('lll')
  return value
})

Vue.filter('formatCommentDate', function (value) {
  if (!value) return ''
  const createdDateDiffSeconds = moment().diff(value, 'seconds');
  /** Show as Seconds */
  let commentDateFormat = { format: 'только что', date_time: '' }

  /** Show as month */
  if(createdDateDiffSeconds > (60 * 60 * 24 * 30)) {
    commentDateFormat = { format: 'мес.', date_time: moment().diff(value, 'months')}
  }
  /** Show as week */
  else if(createdDateDiffSeconds > (60 * 60 * 24 * 7)) {
    commentDateFormat = { format: 'нед.', date_time: moment().diff(value, 'weeks')}
  }
  /** Show as days */
  else if(createdDateDiffSeconds > (60 * 60 * 24)) {
    commentDateFormat = { format: 'д', date_time: moment().diff(value, 'days')}
  }
  /** Show as hours */
  else if(createdDateDiffSeconds > (60 * 60)) {
    commentDateFormat = { format: 'ч.', date_time: moment().diff(value, 'hours') }
  }
  /** Show as minutes */
  else  if(createdDateDiffSeconds > 60) {
    commentDateFormat = { format: 'м.', date_time: moment().diff(value, 'minutes') }
  }
  return `${commentDateFormat.date_time} ${commentDateFormat.format}`
})
