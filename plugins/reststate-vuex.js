import {resourceModule} from '@reststate/vuex'

export default function ({$axios, store}) {
  store.registerModule(
    'projects',
    resourceModule({name: 'projects', httpClient: $axios})
  )

  store.registerModule(
    'activities',
    resourceModule({name: 'activities', httpClient: $axios})
  )

  store.registerModule(
    'news',
    resourceModule({name: 'news', httpClient: $axios})
  )

}
