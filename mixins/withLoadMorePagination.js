export default {
  data() {
    return {
      resource: null,
      loading: true,
      loadingNext: false,
      options: {
        page: 1,
        itemsPerPage: 10,
        sort: ''
      },
      items: [],
      filter: {},
      incrementLoading: true,
    }
  },
  computed: {
    payload() {
      return {
        filter: this.filter,
        options: {
          page: this.options.page,
          itemsPerPage: this.options.itemsPerPage,
          sort: this.options.sort,
        }
      }
    },
    lastPage() {
      return this.lastMeta
        ? this.lastMeta.last_page
        : 0;
    },
    lastMeta() {
      return this.$store.getters[this.resource + "/lastMeta"]
    },
    currentPageItems() {
      return this.$store.getters[this.resource + "/where"](this.payload)
    },
    disableLoadMoreButton() {
      return this.options.page >= this.lastPage
    }
  },

  mounted() {
    this.loadResource()
  },

  methods: {
    async loadResource(pure = false) {
      this.loading = true
      if (pure) {
        this.options.page = 1
      }
      await this.$store.dispatch(this.resource + "/loadWhere", this.payload)
      this.items = this.currentPageItems
      this.loading = false
    },

    async loadNext() {
      if (this.options.page <= this.lastPage) {
        this.loadingNext = true
        this.options.page++
        await this.$store.dispatch(this.resource + "/loadWhere", this.payload)
        this.items = this.items.concat(this.currentPageItems)
        this.loadingNext = false
      }
    },
    searchItems(shouldClearEmptyQuery = false) {
      if (this.filter.search && this.filter.search.length > 0) {
        this.loadResource(true)
      } else if (shouldClearEmptyQuery) {
        this.clearItems()
      } else {
        this.loadResource(true)
      }
    },
    clearItems() {
      this.items = []
    }
  }
}
