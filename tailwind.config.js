module.exports = {
    future: {
        // removeDeprecatedGapUtilities: true,
        // purgeLayersByDefault: true,
    },
    purge: [
        './components/**/*.{vue,js}',
        './layouts/**/*.vue',
        './pages/**/*.vue',
        './plugins/**/*.{js,ts}',
        './nuxt.config.{js,ts}',
    ],
    theme: {
        extend: {
            colors: {
                blue: {
                    primary: '#365E9C',
                    light: '#0575E6',
                },
                gray: {
                  '1': '#212121',
                  '2': '#4F4F4F',
                  '3': '#828282',
                  '4': '#BDBDBD',
                  '5': '#E0E0E0',
                  'primary': '#565656'
                },
                background: '#2A2C34'
            },
            height: {
                '80': '20rem',
                '96': '24rem'
            },
            maxHeight: {
              '34': '34px',
              '48': '48px',
              '350': '350px'
            },
            minHeight: {
                '0': '0',
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                'full': '100%',
                '350': '350px',
            },
            backgroundOpacity: {
                '30': '0.3',
            }
        },
    },
    variants: {},
    plugins: [],
}
