export const getters = {
  newsBySlug: ($store) => slug => $store.news.records.find(news => news.slug === slug),
};
